package com.hw.db.controllers;

import java.sql.Timestamp;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class postDAOSetPostTests {
    @Test
    @DisplayName("Change nothing")
    void SetPostTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        PostDAO.setPost(0, new Post());

        verify(
                mockJdbc,
                never()).update(Mockito.anyString(),
                Mockito.<Object>any()
        );
    }

    @Test
    @DisplayName("Change all fields")
    void SetPostTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);


        Post oldPost = new Post();
        oldPost.setMessage("this is old post");
        oldPost.setCreated(new Timestamp(0));
        oldPost.setAuthor("user");


        Mockito.when(mockJdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt()
        )).thenReturn(oldPost);

        Post newPost = new Post();
        newPost.setMessage("this is new post");
        newPost.setCreated(new Timestamp(1));
        newPost.setAuthor("new user");

        PostDAO.setPost(0, newPost);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq("new user"),
                Mockito.eq("this is new post"),
                Mockito.eq(new Timestamp(1)),
                Mockito.eq(0)
        );
    }

    @Test
    @DisplayName("Change message field")
    void SetPostTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Post oldPost = new Post();
        oldPost.setMessage("this is old post");
        oldPost.setCreated(new Timestamp(0));
        oldPost.setAuthor("user");

        Mockito.when(mockJdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt()
        )).thenReturn(oldPost);

        Post newPost = new Post();
        newPost.setMessage("this is new post");
        PostDAO.setPost(0, newPost);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq("this is new post"),
                Mockito.eq(0)
        );
    }

    @Test
    @DisplayName("Change created field")
    void SetPostTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Post oldPost = new Post();
        oldPost.setMessage("this is old post");
        oldPost.setCreated(new Timestamp(0));
        oldPost.setAuthor("user");

        Mockito.when(mockJdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt()
        )).thenReturn(oldPost);

        Post newPost = new Post();
        newPost.setCreated(new Timestamp(1));
        PostDAO.setPost(0, newPost);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(new Timestamp(1)),
                Mockito.eq(0)
        );
    }
}